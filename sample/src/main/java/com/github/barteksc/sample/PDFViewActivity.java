/**
 * Copyright 2016 Bartosz Schiller
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.barteksc.sample;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.RectF;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnDrawListener;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnPageErrorListener;
import com.github.barteksc.pdfviewer.listener.OnPageScrollListener;
import com.github.barteksc.pdfviewer.listener.OnTapListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.shockwave.pdfium.PdfDocument;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NonConfigurationInstance;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import java.util.List;
import java.util.Timer;

@EActivity(R.layout.activity_main)
@OptionsMenu(R.menu.options)
public class PDFViewActivity extends AppCompatActivity implements OnPageChangeListener, OnLoadCompleteListener,
        OnPageErrorListener, OnDrawListener, OnPageScrollListener {

    public static final int PERMISSION_CODE = 42042;
    public static final String SAMPLE_FILE = "sample.pdf";
    public static final String READ_EXTERNAL_STORAGE = "android.permission.READ_EXTERNAL_STORAGE";
    private static final String TAG = PDFViewActivity.class.getSimpleName();
    private final static int REQUEST_CODE = 42;
    @ViewById
    PDFView pdfView;
    @ViewById
    View signatureView;
    @ViewById
    View signatureAdjustment;
    @ViewById
    View signatureViewConstraint;

    @NonConfigurationInstance
    Uri uri;

    @NonConfigurationInstance
    Integer pageNumber = 0;

    String pdfFileName;

    float positionDX, positionDY;
    float adjustmentX, adjustmentY;

    @OptionsItem(R.id.pickFile)
    void pickFile() {
        int permissionCheck = ContextCompat.checkSelfPermission(this,
                READ_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{READ_EXTERNAL_STORAGE},
                    PERMISSION_CODE
            );

            return;
        }

        launchPicker();
    }

    void launchPicker() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        try {
            startActivityForResult(intent, REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            //alert user that file manager not working
            Toast.makeText(this, R.string.toast_pick_file_error, Toast.LENGTH_SHORT).show();
        }
    }

    @AfterViews
    void afterViews() {
        pdfView.setBackgroundColor(Color.LTGRAY);
        if (uri != null) {
            displayFromUri(uri);
        } else {
            displayFromAsset(SAMPLE_FILE);
        }
        setTitle(pdfFileName);

        configureSignatureView();
    }

    private void configureSignatureView() {
        signatureView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:

                        positionDX = signatureViewConstraint.getX() - event.getRawX();
                        positionDY = signatureViewConstraint.getY() - event.getRawY();
                        break;

                    case MotionEvent.ACTION_MOVE:

                        int x = (int) (event.getRawX() + positionDX);
                        int y = (int) (event.getRawY() + positionDY);
                        int w = signatureView.getWidth();
                        int h = signatureView.getHeight();

                        RectF signatureViewRect = new RectF(x, y, x + w, y + h);

                        int pdfX = (int) pdfView.getX();
                        int pdfY = (int) pdfView.getY();
                        int pdfViewWidth = pdfView.getWidth();
                        int pdfViewHeight = pdfView.getHeight();

                        RectF pdfViewRect = new RectF(pdfX, pdfY, pdfX + pdfViewWidth, pdfY + pdfViewHeight);

                        if (x <= pdfX) {
                            x = pdfX;
                        }
                        if (y <= pdfY) {
                            y = pdfY;
                        }
                        if (signatureViewRect.bottom >= pdfViewRect.bottom) {
                            y = (int) (pdfViewRect.bottom - signatureView.getHeight());
                        }
                        if (signatureViewRect.right >= pdfViewRect.right) {
                            x = (int) (pdfViewRect.right - signatureView.getWidth());
                        }

                        signatureViewConstraint.animate()
                                .x(x)
                                .y(y)
                                .setDuration(0)
                                .start();

                        view.setBackgroundColor(pdfView.arePointsOnPdfPage(new PointF(signatureViewRect.left, signatureViewRect.top), new PointF(signatureViewRect.left, signatureViewRect.bottom), new PointF(signatureViewRect.right, signatureViewRect.top), new PointF(signatureViewRect.right, signatureViewRect.bottom)) ? Color.GREEN : Color.RED);
                        Log.d("TAG", "Page size in pixels width " + pdfView.getWidth() + " height " + pdfView.getHeight());

                        break;
                    default:
                        return false;
                }
                return true;
            }
        });

        signatureAdjustment.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        adjustmentX = event.getRawX();
                        adjustmentY = event.getRawY();

                        break;

                    case MotionEvent.ACTION_MOVE:

                        int x = (int) (event.getRawX() + positionDX);
                        int y = (int) (event.getRawY() + positionDY);
                        int w = signatureView.getWidth();
                        int h = signatureView.getHeight();

                        Log.d(TAG, "Event adjustment X " + adjustmentX + " Y " + adjustmentY);
                        Log.d(TAG, "Event difference in X " + (event.getRawX() - adjustmentX) + " Y " + (event.getRawY() - adjustmentY));

                        break;
                    default:
                        return false;
                }

                return true;
            }
        });
    }

    private void displayFromAsset(String assetFileName) {
        pdfFileName = assetFileName;

        pdfView.fromAsset(assetFileName)
                .defaultPage(pageNumber)
                .onPageChange(this)
                .onPageScroll(this)
                .onDraw(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(this))
                .spacing(10) // in dp
                .onPageError(this)
                .onTap(new OnTapListener() {
                    @Override
                    public boolean onTap(MotionEvent e) {
                        Log.d(TAG, "onTap: Tapped x" + e.getX() + " y" + e.getY());

                        float xPositionInRealScale = pdfView.toRealScale(-pdfView.getCurrentXOffset() - e.getX());
                        float yPositionInRealScale = pdfView.toRealScale(-pdfView.getCurrentYOffset() - e.getY());

//                        float xPositionRelativeToPage = xPositionInRealScale / pdfView.getPageSize(0).getWidth() * 100;
//                        float yPositionRelativeToPage = yPositionInRealScale / pdfView.getPageSize(0).getHeight() * 100;

//                        Log.d(TAG, "onTap: xPositionRelativeToPage " + xPositionRelativeToPage + " yPositionRelativeToPage " + yPositionRelativeToPage);

//                        PointF pageCoordinates = pdfView.getNativeDeviceCoordsToPage(e.getX(), e.getY());

//                        Log.d(TAG, "onTap: Computed page coordinates " + pageCoordinates);

                        return false;
                    }
                })
                .pageFitPolicy(FitPolicy.BOTH)
                .load();
    }

    private void displayFromUri(Uri uri) {
        pdfFileName = getFileName(uri);

        pdfView.fromUri(uri)
                .defaultPage(pageNumber)
                .onPageChange(this)
                .onPageScroll(this)
                .onDraw(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(this))
                .spacing(10) // in dp
                .onPageError(this)
//                .onTap(new OnTapListener() {
//                    @Override
//                    public boolean onTap(MotionEvent e) {
//                        Log.d(TAG, "onTap: Tapped x" + e.getX() + " y" + e.getY());
//
//                        float xPositionInRealScale = pdfView.toRealScale(-pdfView.getCurrentXOffset() - e.getX());
//                        float yPositionInRealScale = pdfView.toRealScale(-pdfView.getCurrentYOffset() - e.getY());
//
//                        float xPositionRelativeToPage = xPositionInRealScale / pdfView.getPageSize(0).getWidth() * 100;
//                        float yPositionRelativeToPage = yPositionInRealScale / pdfView.getPageSize(0).getHeight() * 100;
//
//                        Log.d(TAG, "onTap: xPositionRelativeToPage " + xPositionRelativeToPage + " yPositionRelativeToPage " + yPositionRelativeToPage);
//
//                        PointF pageCoordinates = pdfView.getNativeDeviceCoordsToPage(e.getX(), e.getY());
//
//                        Log.d(TAG, "onTap: Computed page coordinates " + pageCoordinates);
//
//                        return false;
//                    }
//                })
                .pageFitPolicy(FitPolicy.BOTH)
                .load();
    }

    @OnActivityResult(REQUEST_CODE)
    public void onResult(int resultCode, Intent intent) {
        if (resultCode == RESULT_OK) {
            uri = intent.getData();
            displayFromUri(uri);
        }
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));

        Log.d(TAG, "onPageChanged: page " + page + " pageCount " + pageCount);
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        if (result == null) {
            result = uri.getLastPathSegment();
        }
        return result;
    }

    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        Log.e(TAG, "title = " + meta.getTitle());
        Log.e(TAG, "author = " + meta.getAuthor());
        Log.e(TAG, "subject = " + meta.getSubject());
        Log.e(TAG, "keywords = " + meta.getKeywords());
        Log.e(TAG, "creator = " + meta.getCreator());
        Log.e(TAG, "producer = " + meta.getProducer());
        Log.e(TAG, "creationDate = " + meta.getCreationDate());
        Log.e(TAG, "modDate = " + meta.getModDate());

        printBookmarksTree(pdfView.getTableOfContents(), "-");

    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

    /**
     * Listener for response to user permission request
     *
     * @param requestCode  Check that permission request code matches
     * @param permissions  Permissions that requested
     * @param grantResults Whether permissions granted
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CODE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                launchPicker();
            }
        }
    }

    @Override
    public void onPageError(int page, Throwable t) {
        Log.e(TAG, "Cannot load page " + page);
    }

    @Override
    public void onLayerDrawn(Canvas canvas, float pageWidth, float pageHeight, int displayedPage) {
        Log.d(TAG, "onLayerDrawn: pageWidth " + pageWidth + " pageHeight " + pageHeight + " displayPage " + displayedPage);

    }

    @Override
    public void onPageScrolled(int page, float positionOffset) {
        Log.d(TAG, "onPageScrolled: page " + page + " positionOffset " + positionOffset);
    }
}
